#!/bin/bash

echo "Copying fonts, themes, icons and background images to your system"
echo "local or global installation: [(l)ocal/(g)lobal]"
read -r answer
if [ "$answer" = "l" ] || [ "$answer" = "local" ] || [ "$answer" = "L" ] || [ "$answer" = "LOCAL" ]; then
	echo "Installing locally"
  if ! [ -d ~/.icons ]; then
    mkdir ~/.icons
  fi
  cp ./icons/* ~/.icons/
  if ! [ -d ~/.fonts ]; then
    mkdir ~/.fonts
  fi
  cp ./nerdfonts/* ~/.fonts/
  if ! [ -d ~/.themes ]; then
    mkdir ~/.themes
  fi
  cp ./themes/* ~/.themes/
  cp ./images/* ~/.local/share/backgrounds/
  printf "All done!!\nCheck that you have icons, cursors, themes, fonts and background images\nThere are cases where the background images need only be installed locally.\
    If you can't see the images then run: cp ./images/* ~/.local/share/backgrounds/\n"
elif [ "$answer" = "g" ] || [ "$answer" = "global" ] || [ "$answer" = "G" ] || [ "$answer" = "GLOBAL" ]; then
  echo "Installing globally (need sudo permission)"
  sudo cp -r ./icons/* /usr/share/icons/
  sudo cp -r ./themes/* /usr/share/themes/
  sudo cp -r ./nerdfonts/* /usr/share/fonts/
  sudo cp ./images/* /usr/share/backgrounds/
  printf "All done!!\nCheck that you have icons, cursors, themes, fonts and background images\nThere are cases where the background images need only be installed locally.\
    If you can't see the images then run: cp ./images/* ~/.local/share/backgrounds/\n"
fi;

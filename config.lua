--
--- key_mappings start ---

vim.opt.clipboard = "unnamedplus"
vim.opt.cmdheight = 1
vim.opt.scrolloff = 3
vim.opt.expandtab = true -- convert tabs to spaces
vim.opt.shiftwidth = 2   -- the number of spaces inserted for each indentation
vim.opt.tabstop = 2      -- insert 4 spaces for a tab
vim.opt.relativenumber = true
vim.opt.mousemodel = "extend"

lvim.log.level = "warn"
lvim.format_on_save.enabled = true
lvim.colorscheme = "tokyonight-moon"

lvim.leader = "space"

vim.diagnostic.config({ virtual_text = false })

-- add your own keymapping
lvim.keys.insert_mode["jj"] = "<esc>"
--
lvim.keys.normal_mode["ι"] = ":startinsert<cr>" -- otan exw ellhnika na mh xreiazetai na allaksw gia insert mode
lvim.keys.normal_mode["α"] = "<Right>:startinsert<cr>"
lvim.keys.insert_mode["<C-σ>"] = "<esc>:w<cr>"
lvim.keys.normal_mode["<C-σ>"] = ":w<cr>"

lvim.keys.insert_mode["<C-BS>"] = "<C-w>"
lvim.keys.normal_mode["<C-s>"] = ":w<cr>"
lvim.keys.insert_mode["<C-s>"] = "<esc>:w<cr>"

lvim.keys.normal_mode["<C-n>"] = ":BufferLineCycleNext<cr>"
lvim.keys.insert_mode["<C-n>"] = "<escape>:BufferLineCycleNext<cr>"
lvim.keys.visual_mode["<C-n>"] = "<escape>:BufferLineCycleNext<cr>"
lvim.keys.normal_mode["<C-b>"] = ":BufferLineCyclePrev<cr>"
lvim.keys.insert_mode["<C-b>"] = "<escape>:BufferLineCyclePrev<cr>"
lvim.keys.visual_mode["<C-b>"] = "<escape>:BufferLineCyclePrev<cr>"

lvim.keys.normal_mode["<A-n>"] = ":BufferLineCycleNext<cr>"
lvim.keys.insert_mode["<A-n>"] = "<escape>:BufferLineCycleNext<cr>"
lvim.keys.visual_mode["<A-n>"] = "<escape>:BufferLineCycleNext<cr>"
lvim.keys.normal_mode["<A-b>"] = ":BufferLineCyclePrev<cr>"
lvim.keys.insert_mode["<A-b>"] = "<escape>:BufferLineCyclePrev<cr>"
lvim.keys.visual_mode["<A-b>"] = "<escape>:BufferLineCyclePrev<cr>"

lvim.keys.normal_mode["<A-c>"] = ":BufferKill<cr>"
lvim.keys.normal_mode["<C-c>"] = ":BufferKill<cr>"

lvim.keys.normal_mode["<S-h>"] = ":BufferLineMovePrev<cr>"
lvim.keys.normal_mode["<S-l>"] = ":BufferLineMoveNext<cr>"

lvim.keys.insert_mode["<C-v>"] = "<C-R>+"
lvim.keys.visual_mode["<C-v>"] = "<C-R>+"

lvim.keys.visual_mode["<C-c>"] = "y"

lvim.keys.normal_mode["<C-z>"] = "u"
lvim.keys.visual_mode["<C-z>"] = "<escape>u"
lvim.keys.insert_mode["<C-z>"] = "<escape>u"

lvim.keys.normal_mode["<C-q>"] = "<C-v>"           -- visual block

lvim.keys.normal_mode["<space>rw"] = ":cd %:h<cr>" -- change working directory to the opened file dir

lvim.keys.normal_mode["<C-LeftMouse>"] = "<C-v>"

-- delete without keeping the text in a buffer
lvim.keys.normal_mode["m"] = "\"_"
lvim.keys.visual_mode["m"] = "\"_"

-- mappings to swap consecutive lines. current,down - current,up respectively
lvim.keys.normal_mode["<A-down>"] = "\"1dd\"1p"
lvim.keys.normal_mode["<A-up>"] = "\"1ddk\"1P"

-- lvim.builtin.terminal.start_in_insert = false
vim.api.nvim_set_keymap("t", "hh", "<C-\\><C-n>", { noremap = true, silent = true })

--- key_mappings end ---

--- neovide config start ---
if vim.g.neovide == true then
  -- start of badly interpreted hex values for terminal colors
  vim.g.terminal_color_0 = '#130738'
  vim.g.terminal_color_1 = '#cc144b'
  vim.g.terminal_color_2 = '#38E669'
  vim.g.terminal_color_3 = '#FFC04B'
  vim.g.terminal_color_4 = '#22e816'
  vim.g.terminal_color_5 = '#f8fc2d'
  vim.g.terminal_color_6 = '#107887'
  vim.g.terminal_color_7 = '#5E7F1B'
  vim.g.terminal_color_8 = '#8365dd'
  vim.g.terminal_color_9 = '#7c13bd'
  vim.g.terminal_color_10 = '#90F32D'
  vim.g.terminal_color_11 = '#ffa11a'
  vim.g.terminal_color_12 = '#4699ff'
  vim.g.terminal_color_13 = '#de2bf3'
  vim.g.terminal_color_14 = '#f19302'
  vim.g.terminal_color_15 = '#e5f2e3'
  -- end of badly interpreted hex values for terminal colors

  vim.g.neovide_refresh_rate = 60
  vim.g.neovide_refresh_rate_idle = 5
  vim.g.neovide_scroll_animation_length = 0.5
  vim.g.neovide_remember_window_size = 1
  vim.g.neovide_transparency = 1.00
  vim.g.neovide_cursor_vfx_mode = "railgun"
  vim.g.neovide_confirm_quit = 1
  vim.g.neovide_no_idle = 0
  vim.g.neovide_fullscreen = 0
  -- vim.g.neovide_profiler = 1-- sets the stats in the top left corner
  -- vim.g.neovide_touch_deadzone = 6.0
  vim.g.neovide_cursor_animation_length = 0.15
  vim.g.neovide_cursor_trail_size = 0.17
  vim.g.neovide_cursor_antialiasing = 1
  vim.g.neovide_cursor_unfocused_outline_width = 0.125
  vim.g.neovide_cursor_vfx_particle_lifetime = 0.0 -- no particles for speed
  vim.g.neovide_cursor_vfx_particle_density = 10
  vim.g.neovide_cursor_vfx_opacity = 60


  local opts = { noremap = true, silent = true }

  -- dyn cha transp start
  ChangeTransp = function(delta)
    local delta_trp = vim.g.neovide_transparency + delta
    vim.fn.execute("let g:neovide_transparency=" .. tostring(delta_trp))
  end

  vim.keymap.set({ 'n', 'i' }, "<A-S-M>", function() ChangeTransp(0.05) end, opts)
  vim.keymap.set({ 'n', 'i' }, "<A-m>", function() ChangeTransp(-0.05) end, opts)
  -- dyn cha transp end

  -- dynamically change the font size begin
  vim.g.gui_font_default_size = 12
  vim.g.gui_font_size = vim.g.gui_font_default_size
  vim.g.gui_font_face = "FiraCode Nerd Font"

  RefreshGuiFont = function()
    vim.opt.guifont = string.format("%s:h%s", vim.g.gui_font_face, vim.g.gui_font_size)
  end

  ResizeGuiFont = function(delta)
    vim.g.gui_font_size = vim.g.gui_font_size + delta
    RefreshGuiFont()
  end

  ResetGuiFont = function()
    vim.g.gui_font_size = vim.g.gui_font_default_size
    RefreshGuiFont()
  end
  -- Call function on startup to set default value
  ResetGuiFont()

  -- Keymaps
  vim.keymap.set({ 'n', 'i' }, "<C-+>", function() ResizeGuiFont(1) end, opts)
  vim.keymap.set({ 'n', 'i' }, "<C-->", function() ResizeGuiFont(-1) end, opts)
  -- dynamically change the font size end
end

--- neovide config end ---


--- Plugin configurations ---

--- lualine start ---
local components = require("lvim.core.lualine.components")
lvim.builtin.lualine.options.theme = "auto"

lvim.builtin.lualine.options.section_separators = { left = "", right = "" }
lvim.builtin.lualine.options.component_separators = "|"

lvim.builtin.lualine.sections = {
  lualine_a = { { function() return '󰀘 ' end, separator = { left = "" }, right_padding = 2 }, { 'mode', separator = { right = "", left = "" } } },
  lualine_b = { 'fileformat', components.encoding, components.python_env },
  lualine_c = {
    components.filename,
    components.filetype,
    components.lsp,
    components.diagnostics
  },
  lualine_x = {
    components.diff,
    components.branch,
  },
  lualine_y = { components.spaces, 'location', 'progress' },
  lualine_z = {
    { separator = { right = '' }, left_padding = 2, function() return os.date('  %m-%d |   %H:%M') end },
  },
}
--- lualine end ---

-- -- set a formatter, this will override the language server formatting capabilities (if it exists)
local formatters = require "lvim.lsp.null-ls.formatters"
formatters.setup {
  {
    command = "black",
    filetypes = { "python", },
  },
  {
    -- each formatter accepts a list of options identical to https://github.com/jose-elias-alvarez/null-ls.nvim/blob/main/doc/BUILTINS.md#Configuration
    command = "prettier",
    ---@usage arguments to pass to the formatter
    -- these cannot contain whitespaces, options such as `--line-width 80` become either `{'--line-width', '80'}` or `{'--line-width=80'}`
    extra_args = { "--print-width", "100" },
    ---@usage specify which filetypes to enable. By default a providers will attach to all the filetypes it supports.
    -- filetypes = { "typescript", "javascript" --[[ , "typescriptreact" ]] },
    ---@brief disable all filetypes for by default installed formatter prettier because prettier sucks. It's not pretty
    disabled_filetypes = { "javascript", "typescript", "flow", "jsx", "json", "css", "scss", "less", "html", "vue",
      "angular", "graphql", "markdown", "yaml" },
  },
  {
    command = "clang-format",
    ---@usage specify which filetypes to enable. By default a providers will attach to all the filetypes it supports.
    filetypes = { "typescript", "javascript" },
  },
  {
    command = "yamlfmt",
    ---@usage specify which filetypes to enable. By default a providers will attach to all the filetypes it supports.
    filetypes = { --[[ "yaml", ]] "json" },
  },
}

-- -- set additional linters
local linters = require "lvim.lsp.null-ls.linters"
linters.setup {
  -- {
  --   -- each linter accepts a list of options identical to https://github.com/jose-elias-alvarez/null-ls.nvim/blob/main/doc/BUILTINS.md#Configuration
  --   command = "shellcheck",
  --   ---@usage arguments to pass to the formatter
  --   -- these cannot contain whitespaces, options such as `--line-width 80` become either `{'--line-width', '80'}` or `{'--line-width=80'}`
  --   extra_args = { "--severity", "warning" },
  -- },
  -- {
  --   command = "protolint",
  --   ---@usage specify which filetypes to enable. By default a providers will attach to all the filetypes it supports.
  --   filetypes = { "proto" },
  --   extra_args = { "-fix" },
  -- },
}


local dap = require('dap')

dap.adapters.lldb = {
  type = 'executable',
  command = '/usr/bin/lldb-vscode', -- adjust as needed, must be absolute path
  name = 'lldb'
}

dap.configurations.c = {
  {
    name = 'Launch',
    type = 'lldb',
    request = 'launch',
    program = function()
      return vim.fn.input('Path to executable: ' .. vim.fn.getcwd() .. '/')
    end,
    stopOnEntry = false,
    cwd = '${workspaceFolder}',
    args = function()
      local args = {}
      local args_string = vim.fn.input('Arguments: ')
      for word in args_string:gmatch("%S+") do
        table.insert(args, word)
      end
      return args
      -- local args_string = vim.fn.input('Arguments: ')
      -- return vim.split(args_string, "\n", { trimepty = true })
    end,
    -- runInTerminal = false,
    env = function()
      local variables = {}
      for k, v in pairs(vim.fn.environ()) do
        table.insert(variables, string.format("%s=%s", k, v))
      end
      return variables
    end,
  },
}
dap.configurations.cpp = dap.configurations.c
dap.configurations.rust = dap.configurations.c

dap.adapters.go = {
  type = 'executable',
  command = os.getenv("HOME") .. '/.local/share/lvim/mason/bin/go-debug-adapter', -- adjust as needed, must be absolute path
  name = 'go'
}

dap.configurations.go = {
  {
    name = "Launch",
    type = "go",
    request = "launch",
    showLog = true,
    cwd = "${workspaceFolder}",
    program = function()
      return "${workspaceFolder}" -- vim.fn.input({ 'Path to executable: ' .. vim.fn.getcwd() .. '/', 'file' })
    end,
    -- stopOnEntry = true,
    args = function()
      local args_string = vim.fn.input('Arguments: ')
      return vim.split(args_string, "\n", { trimepty = true })
    end,
    -- runInTerminal = false,
    -- trace = "verbose",
    env = function()
      local variables = {}
      for k, v in pairs(vim.fn.environ()) do
        table.insert(variables, string.format("%s=%s", k, v))
      end
      return variables
    end,
    dlvToolPath = os.getenv("HOME") .. "/.local/share/lvim/mason/packages/delve/dlv"
  },
}

-- dap-adapter config end

lvim.keys.normal_mode["<A-K>"] = ":lua require('dapui').eval()<cr>"

lvim.keys.normal_mode["<A-o>"] = ":DapStepOver<cr>"

lvim.keys.normal_mode["<A-i>"] = ":DapStepInto<cr>"

--- custom autocommand start ---
vim.api.nvim_create_autocmd("BufReadPost", {
  group = vim.api.nvim_create_augroup("lazyvim_last_loc", { clear = true }),
  callback = function(event)
    local exclude = { "gitcommit" }
    local buf = event.buf
    if vim.tbl_contains(exclude, vim.bo[buf].filetype) or vim.b[buf].lazyvim_last_loc then
      return
    end
    vim.b[buf].lazyvim_last_loc = true
    local mark = vim.api.nvim_buf_get_mark(buf, '"')
    local lcount = vim.api.nvim_buf_line_count(buf)
    if mark[1] > 0 and mark[1] <= lcount then
      pcall(vim.api.nvim_win_set_cursor, 0, mark)
    end
  end,
})
--- custom autocommand end ---

local which_key_setup = lvim.builtin.which_key.setup
which_key_setup.plugins.marks = true     -- show marks when pressing ' or ` in normal
which_key_setup.plugins.registers = true -- show register values when @ or " in normal
which_key_setup.plugins.presets.z = true -- show the zipline help bcz i dont remember it

local which_key_mappings = lvim.builtin.which_key.mappings
which_key_mappings["t"] = {
  name = "+Trouble",
  r = { "<cmd>Trouble lsp_references<cr>", "References" },
  f = { "<cmd>Trouble lsp_definitions<cr>", "Definitions" },
  d = { "<cmd>Trouble document_diagnostics<cr>", "Diagnostics" },
  q = { "<cmd>Trouble quickfix<cr>", "QuickFix" },
  l = { "<cmd>Trouble loclist<cr>", "LocationList" },
  w = { "<cmd>Trouble workspace_diagnostics<cr>", "Workspace Diagnostics" },
}
which_key_mappings["r"] = {
  name = "Restore Session",
  s = { "<cmd>lua require('persistence').load()<cr>", "Restore the session for the current directory" },
  l = { "<cmd>lua require('persistence').load({ last = true })<cr>", "Restore the last session" },
  d = { "<cmd>lua require('persistence').stop()<cr>", "Stop Persistence => session won't be saved on exit" }
}

which_key_mappings["S"] = { { ':lua require("spectre").toggle({select_word=true})<cr>', "Toggle Spectre" } }
which_key_mappings["s"]["w"] = { '<esc><cmd>lua require("spectre").open_visual({select_word=true})<CR>',
  "Search current word" }

lvim.builtin.which_key.vmappings["s"] = {
  name = "Spectre",
  w = { '<esc><cmd>lua require("spectre").open_visual()<CR>', "Search current word" }
}
which_key_mappings["s"]["p"] = { '<cmd>lua require("spectre").open_file_search({select_word=true})<CR>',
  "Search on current file" }
-- require("spectre.config").mapping["send_to_qf"].map = "<C-q>"

table.insert(lvim.builtin.alpha.dashboard.section.buttons.entries, 3,
  { "s", "  Restore Session", "<cmd>lua require('persistence').load({ last = true })<cr>" })
lvim.builtin.alpha.dashboard.section.buttons.entries[5][2] = "  Recent files"
--
which_key_mappings["c"] = {}

--- Plugin configuration end ---
-- require('lspconfig').taplo.setup {
--   settings = {
--     evenBetterToml = {
--       schema = { catalogs = { "https://taplo.tamasfe.dev/schema_index.json" } },
--     },
--   },
-- }

require('lspconfig').yamlls.setup {
  settings = {
    yaml = {
      validate = true,
      -- disable the schema store
      schemaStore = {
        enable = false,
        url = "",
      },
      schemas = {
        -- ['https://json.schemastore.org/github-workflow.json'] = '.github/workflows/*.{yml,yaml}',
        -- ["https://raw.githubusercontent.com/yannh/kubernetes-json-schema/master/v1.22.4-standalone-strict/all.json"] = "*.yml",
        -- ["kubernetes"] = "*.yml",
      }
    }
  }
}

lvim.builtin.lir.active = false -- lir sucks

lvim.plugins = {
  {
    "folke/trouble.nvim",
    cmd = "TroubleToggle",
  },
  {
    "ray-x/lsp_signature.nvim",
    -- config = require("lsp_signature").setup()
  },
  { "tpope/vim-surround" },
  {
    "nvim-pack/nvim-spectre",
    dependencies = { 'nvim-lua/plenary.nvim' },
    -- config = {
    --   default = {
    --     replace = {
    --       cmd = "oxi"
    --     }
    --   }
    -- } -- after running build.sh in nvim-spectre installation dir
  },
  { "BurntSushi/ripgrep" },
  { "nvim-telescope/telescope-dap.nvim" },
  { "theHamsta/nvim-dap-virtual-text" },
  {
    "folke/persistence.nvim",
    event = "BufReadPre", -- this will only start session saving when an actual file was opened
    -- config = require("persistence").setup()
  }
}

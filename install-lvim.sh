#!/bin/bash

echo 'Ensure installed: [git, python, pip, npm, node, cargo, Neovim >= 0.9.0]'
echo 'Optional (but do them): [a nerdfont, kitty like terminal, ripgrep, fd]'
echo 'Proceed with installation? [Y/n]'
read -r confirm
if [ "$confirm" != "y" ] && [ "$confirm" != "Y" ] && [ "$confirm" != "yes" ] && [ "$confirm" != "YES" ]; then
	echo 'Answer implied as no'
	exit 0
fi

if ! [ -d "$HOME/.npm-global" ]; then
	mkdir ~/.npm-global
fi

echo 'Proceeding with installation...'
set -eum

# # Either
# export NPM_CONFIG_PREFIX=~/.npm-global
# # Or
npm config set prefix "$HOME/.npm-global"
echo "export PATH=~/.npm-global/bin:$PATH" >> ~/.profile
source "$HOME/.profile"

LV_BRANCH='release-1.3/neovim-0.9' bash <(curl -s https://raw.githubusercontent.com/LunarVim/LunarVim/release-1.3/neovim-0.9/utils/installer/install.sh)
echo "copying local config.lua to lvim config.lua"
cp ./config.lua ~/.config/lvim/config.lua
echo "copying local telescope.lua to lvim core plugins telescope.lua"
cp ./telescope.lua ~/.local/share/lunarvim/lvim/lua/lvim/core/telescope.lua
